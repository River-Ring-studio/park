﻿using System;
using UnityEngine;

public class LightController : MonoBehaviour
{
	public Light Light;
	public float Distance = 10f;
	private GameObject _player;
	private float _intensity;
	private float _maxIntensity = 5;
	private float _speed = 3;
	
	void Start ()
	{
		_player = GameObject.FindWithTag("Player") as GameObject;
	}
	
	void Update ()
	{
		float currentDist = Mathf.Round(Vector3.Distance(transform.position, _player.transform.position));

		if (currentDist > Distance) {
			_intensity = 0.0f;
		}
		else {
			_intensity = (1.0f - currentDist / Distance) * _maxIntensity;
		} 
     
		Light.intensity = Mathf.Lerp(Light.intensity, _intensity, _speed * Time.deltaTime);
	}
}
