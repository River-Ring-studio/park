﻿using System;
using System.Collections;
using UnityEngine;

namespace StandScripts
{
    public class TextureLoader : MonoBehaviour
    {
        private Texture2D _texture;
        private bool _inProgress = false;
        private int _lastInQueue = 0;
        private int _currentQueue = 1;
        private IEnumerator _Load(string url, Action callback)
        {
            _texture = new Texture2D(4, 4, TextureFormat.DXT1, false);

            WWW www = new WWW(url);
            while (!www.isDone)
            {
                yield return null;
            }
            www.LoadImageIntoTexture(_texture);
            callback();
        }

        public void Load(string url, Texture2DCallback callback)
        {
            _lastInQueue++;
            StartCoroutine(LoadAtQueue(_lastInQueue, url, callback));
        }

        private IEnumerator LoadAtQueue(int numberInQueue, string url, Texture2DCallback callback)
        {
            while (_inProgress || numberInQueue != _currentQueue)
            {
                yield return null;
            }
            
            _inProgress = true;
            StartCoroutine(_Load(url, delegate
            {
                _inProgress = false;
                _currentQueue++;
                callback(_texture);                
            }));
        }
    }
    
    public delegate void Texture2DCallback(Texture2D texture);
}