﻿using UnityEngine;
using UnityEngine.UI;

namespace StandScripts
{
	public class MainUi : MonoBehaviour
	{

		public void Update()
		{
			if (Input.GetKeyDown(KeyCode.Escape))
			{
				HideSlide();
			}
		}
		
		public GameObject Slide;
		
		public void ShowSlide(Texture2D texture)
		{
			Slide.SetActive(true);
			Slide.GetComponent<RawImage>().texture = texture;
		}
		
		public void HideSlide()
		{
			Slide.SetActive(false);
		}
	}
}
