﻿using UnityEngine;

namespace StandScripts
{
	public class Arrows : MonoBehaviour
	{
		public GameObject Foreground;

		public void ShowForeground()
		{
			Foreground.SetActive(true);
		}
		
		public void HideForeground()
		{
			Foreground.SetActive(false);
		}
	}
}
