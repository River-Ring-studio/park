﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection.Emit;
using UnityEngine;
using UnityEngine.UI;

public class QuizManager : MonoBehaviour
{

	public Toggle Answer1;
	public Toggle Answer2;
	public Toggle Answer3;
	public Toggle Answer4;
	public Text AnswerCounterText;
	public Text CorrectAnswerText;
	public Text WrongAnswerText;

	private Toggle _rightAnswer;
	private bool _isPressed = false;
	private int _answerCount = 0;
	private int _correctAnswersCount = 0;
	private int _wrongAnswersCount = 0;

	private bool _isNear = false;
//	// Use this for initialization
	void Start ()
	{
		_rightAnswer = Answer2;
	}
//	
//	// Update is called once per frame
//	void Update () {
//		 
//	}
	
	void OnGUI() {
		if (!_isPressed && _isNear)
		{
			Event e = Event.current;
			if (e.isKey)
			{
				switch (e.keyCode)
				{
					case KeyCode.Alpha1:
						IncreaseAnswersCounter(Answer1);
						break;
					case KeyCode.Alpha2:
						IncreaseAnswersCounter(Answer2);
						break;
					case KeyCode.Alpha3:
						IncreaseAnswersCounter(Answer3);
						break;
					case KeyCode.Alpha4:
						IncreaseAnswersCounter(Answer4);
						break;
				}
			}
		}
	}

	void IncreaseAnswersCounter(Toggle toggle)
	{
		toggle.isOn = true;
		_isPressed = true;
		_answerCount++;
		AnswerCounterText.text = "Answers: " + _answerCount;
		checkAnswer(toggle);
	}

	void checkAnswer(Toggle toggle)
	{
		if (toggle == _rightAnswer)
		{
			Debug.Log("Here");
			toggle.GetComponentInChildren<Text>().color = Color.green;
			_correctAnswersCount++;
			CorrectAnswerText.text = "Correct answers: " + _correctAnswersCount;
		}
		else
		{
			toggle.GetComponentInChildren<Text>().color = Color.red;
			_wrongAnswersCount++;
			WrongAnswerText.text = "Wrong answers: " + _wrongAnswersCount;
			_rightAnswer.GetComponentInChildren<Text>().color = Color.green;
		}
	}
	
	public bool IsNear
	{
		get { return _isNear; }
		set { _isNear = value; }
	}
}
