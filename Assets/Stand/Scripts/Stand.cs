﻿using System;
using System.Net;
using UnityEngine;

namespace StandScripts
{
	public class Stand : MonoBehaviour
	{
		public string Name;
		public float ShowUiDistance = 1000f;
		public float HideUiDistance = 1500f;
		public float StartLoadingDistance = 4000f;
		public Transform PlayerObject;
		public Canvas StandUiObject;
		public Canvas MainUiObject;
		public GameObject ArrowLeft;
		public GameObject ArrowRight;
		public TextureLoader TextureLoader;
		public string Url = "http://localhost/stands/";
		public GameObject SlideObject;
		public GameObject SignObject;
		public GameObject BackSlideObject;
		public GameObject BackSignObject;
		
		private bool _initialized = false;
		private IndexFormat _slidesIndex;
		private int _currentSlideNumber;
		private bool _isSlideLoading = false;

		public void Update ()
		{
			float distance = Vector3.Distance(transform.position, PlayerObject.position);
			if (!StandUiObject.gameObject.activeSelf && distance <= ShowUiDistance)
			{
				StandUiObject.gameObject.SetActive(false);
			}
			else if (StandUiObject.gameObject.activeSelf && distance >= HideUiDistance)
			{
				StandUiObject.gameObject.SetActive(false);
			}

			if (!_initialized && distance <= StartLoadingDistance)
			{
				_initialized = true;
				LoadIndex();
				LoadSign();
				LoadFirstSlide();
			}
		}

		public void SwitchToNextSlide()
		{
			if (!_isSlideLoading)
			{
				_isSlideLoading = true;
				LoadSlide(_currentSlideNumber + 1, delegate
				{
					ArrowLeft.GetComponent<Arrows>().HideForeground();
					if (_slidesIndex.Slides.Length == _currentSlideNumber + 1)
					{
						ArrowRight.GetComponent<Arrows>().ShowForeground();
					}
					_isSlideLoading = false;
				});
			}
		}
		
		public void SwitchToPreviousSlide()
		{
			if (!_isSlideLoading)
			{
				_isSlideLoading = true;
				LoadSlide(_currentSlideNumber - 1, delegate
				{
					ArrowRight.GetComponent<Arrows>().HideForeground();
					if (_currentSlideNumber == 0)
					{
						ArrowLeft.GetComponent<Arrows>().ShowForeground();
					}
					_isSlideLoading = false;
				});
			}
		}

		public void BringClose()
		{
			MainUiObject.GetComponent<MainUi>().ShowSlide((Texture2D) SlideObject.GetComponent<Renderer>().material.mainTexture);
		}
		
		private void LoadSign()
		{
			string url = Url + "/" + Name + "/" + _slidesIndex.Sign;

			TextureLoader.GetComponent<TextureLoader>().Load(url, delegate(Texture2D texture)
			{
				SignObject.GetComponent<Renderer>().material.mainTexture = texture;
				BackSignObject.GetComponent<Renderer>().material.mainTexture = texture;
			
				SignObject.GetComponent<Renderer>().material.SetTexture ("_EmissionMap", texture);
				BackSignObject.GetComponent<Renderer>().material.SetTexture ("_EmissionMap", texture);				
			});
		}

		private void LoadIndex()
		{
			string indexUrl = Url + "/" + Name + "/index.json";
			var json = new WebClient().DownloadString(indexUrl);
			_slidesIndex = JsonUtility.FromJson<IndexFormat>(json);
		}

		private void LoadFirstSlide()
		{
			LoadSlide(0);
		}

		private void LoadSlide(int number, Action callback = null)
		{
			_currentSlideNumber = number;
			
			string url = Url + "/" + Name + "/" + _slidesIndex.Slides[number];

			TextureLoader.GetComponent<TextureLoader>().Load(url, delegate(Texture2D texture)
			{
				SlideObject.GetComponent<Renderer>().material.mainTexture = texture;
				BackSlideObject.GetComponent<Renderer>().material.mainTexture = texture;

				SlideObject.GetComponent<Renderer>().material.SetTexture("_EmissionMap", texture);
				BackSlideObject.GetComponent<Renderer>().material.SetTexture("_EmissionMap", texture);
				
				if (callback != null)
				{
					callback();
				}
			});
		}
	}

	[Serializable]
	internal class IndexFormat
	{
		public string[] Slides;
		public string Sign;
	}
}