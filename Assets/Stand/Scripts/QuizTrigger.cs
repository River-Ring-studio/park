﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuizTrigger : MonoBehaviour
{
	public QuizManager QuizManager;
	
	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag("Player"))
		{
			QuizManager.IsNear = true;
			Debug.Log("Enter");
		}
	}

	private void OnTriggerExit(Collider other)
	{
		if (other.gameObject.CompareTag("Player"))
		{
			QuizManager.IsNear = false;
			Debug.Log("Exit");
		}
	}
}
